# Working Group Outreach

This working group has a focus on increasing visiblity of the ETH community and has no defined end.

## Some ideas

- Organize regular website updates, news section?
- Monthyl / Quarterly newsletter?
- Improve website: content, structure, accessability (a11y).
- Chat moderation? Code of Conduct? Welcome message bot?
- Organize regular community mail to mailing list (monthly?)
- How to organize RSE ambassadors in departments
    - Unified presentation slides?
- Work on unified presentation to present RSE community.
- Get in contact with ETH communications ('ETH news' article?)
- Approach department IT groups
- Contact  https://www.vmitet.ethz.ch/ ?
    - Are there similar associations for "Mittelstand" in other departments?
